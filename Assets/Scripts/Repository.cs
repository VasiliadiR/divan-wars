﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Repository : MonoBehaviour
{
    public Text mainExpText;
    public Text mainGoldText;
    public Text levelText;
    public ParticleSystem highlander;
    private static ParticleSystem _highlander;

    private static GameObject[] _plusButtons;
    private static GameObject[] _upButtons;

    private static float gold = 0;
    private static float expirience = 0;
    private static float levelExpirience = 0;
    private static int prevLevel = 0;
    private static int level = 0;
    
    
    public void TestGoldPlus() // TEST!!!!!!!!!!
    {
        GoldChange(10);
    }
    public void TestExpPlus() // TEST!!!!!!!!!!
    {
        ExpChange(10);
    }
    private void Start()    //скрпит хранит данные о золоте и опыте
    {
        _plusButtons = GameObject.FindGameObjectsWithTag("PlusButton");
        _upButtons = GameObject.FindGameObjectsWithTag("UpButton");
        InteractableButton(_plusButtons, false);
        InteractableButton(_upButtons, false);
        _highlander = highlander;
    }
    private void FixedUpdate()
    {
        mainExpText.text = expirience.ToString();
        mainGoldText.text = gold.ToString();
        levelText.text = level.ToString();

        level = (int)levelExpirience / 20;  // повышение уровня Аза
        if (prevLevel != level)
        {
            _highlander.Play();     //анимация повышения уровня
            prevLevel = level;
        }
    }
    public static void ExpChange(float val) // интерактивность кнопок улучшения супероружия 
    {
        expirience += val;
        if (val > 0)
            levelExpirience += val;
        if (expirience >= 20)
            InteractableButton(_plusButtons, true);
        else if (expirience < 20)
            InteractableButton(_plusButtons, false);
    }
    public static void GoldChange(float val)    // интерактивность кнопок улучшения скиллов 
    {
        gold += val;
        if (gold == 10)
            InteractableButton(_upButtons, true);
        else if (gold < 10)
            InteractableButton(_upButtons, false);
    }
    private static void InteractableButton(GameObject[] buttons, bool trig)
    {
        foreach (var but in buttons)
            but.GetComponent<Button>().interactable = trig;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class EnemiesOnMap : MonoBehaviour
{
    public static event Action EnemyIn;
    public static event Action EnemyOut;
    public Text timeText;
    private float time;

    private bool enemyTrig = true;

    private void FixedUpdate()  //скрипт регистрирует наличие противников на поле бое, и вызывает события по появлению и уничтожению
    {
        GameObject[] units = GameObject.FindGameObjectsWithTag("Enemy");
        if (units.Length != 0) // враги есть
        {
            if (!enemyTrig)
            {
                EnemyIn();
                enemyTrig = true;
                timeText.enabled = false;
            }
        }
        else if (enemyTrig)    // врагов нет
        {
            EnemyOut();
            enemyTrig = false;
            time = 30;
            timeText.enabled = true;           
        }
        time -= Time.deltaTime; // выводит на экран время, спустя которое начнется вторая волна противников 
        timeText.text = time.ToString();
    }
}

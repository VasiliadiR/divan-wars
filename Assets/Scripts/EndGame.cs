﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    public Text endGameText;

    private void Start() // Скрипт отвечает за вывод сообщений о конце игры
    {
        Defence.EnemyWon += Defeat;
        EnemyRespawn.WarIsOver += Victory;
    }
    private void Victory()
    {
        endGameText.text = "Congratulations, tacher!";
        endGameText.enabled = true;
    }
    private void Defeat()
    {
        endGameText.text = "That's all Folks!";
        endGameText.enabled = true;
    }


}

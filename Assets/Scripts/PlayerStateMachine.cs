﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerStateMachine : BaseStateMachine
{
    private Selection _selection;

    public override void Start()
    {
        base.Start();
        staticTarget = GameObject.Find("Fountain");
        _selection = gameObject.GetComponent<Selection>();
        _oppositeTag = "Enemy"; // выбор поискового тэга
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (_selection.enemy != null) // выбранный противник
        {
            _selection.moveToPoint = false;
            _closest = _selection.enemy;
            if (!_selection.enemy.activeSelf)
                _selection.enemy = null;
        }
    }
    public override void GoToStaticTarget(GameObject staticTarget) // к статичному объекту (фонтану)
    {
        agent.stoppingDistance = 9;
        agent.SetDestination(staticTarget.transform.position);

        if (_selection.moveToPoint) // к точке
            CurrentState = GoToPoint;
        else if (_closest != null)  // к ближайшему противнику
        {
            _currentTarget = _closest;
            CurrentState = GoToEnemy;
        }
        else if (Vector3.Distance(agent.destination, gameObject.transform.position) < agent.stoppingDistance)   //сближение со статическим объектом
            CurrentState = Repair;      // восстановить здоровье     
    }
    public override void GoToEnemy(GameObject target) // к ближайшему или выбранному противнику
    {
        target.GetComponent<EnemiesInSight>().Shine(gameObject); // подсветка противника, к которому идет юнит
        _currentTarget = _closest;
        agent.stoppingDistance = range;
        agent.SetDestination(target.transform.position);

        if (_selection.moveToPoint) // к точке
            CurrentState = GoToPoint;
        else if (_closest == null)  // к ближайшему противнику
        {
            CurrentState = GoToStaticTarget;
            _currentTarget = staticTarget;
        }
        else if (Vector3.Distance(agent.destination, gameObject.transform.position) < agent.stoppingDistance + 1)   // при достаточном сближении с целью - атаковать
        {
            CurrentState = Attack;
            _currentTarget = _closest;
            _selection.enemy = null;
        }
    }
    public override void Attack(GameObject target)  // атака противника
    {
        target.GetComponent<EnemiesInSight>().Shine(gameObject);   // подсветка противника, которого атакует юнит
        agent.SetDestination(target.transform.position);

        if (_selection.moveToPoint) // к точке
            CurrentState = GoToPoint;
        else if (Vector3.Distance(agent.destination, gameObject.transform.position) >= agent.stoppingDistance + 1)  // к тому же противнику при увеличении дистанции
        {
            CurrentState = GoToEnemy;
            _currentTarget = target;
        }
        else if (_selection.enemy != null && target != _selection.enemy)    // атаковать выбранного противника, если он дальше, то при следующем цикле состояние будет GoToEnemy
        {
            CurrentState = Attack;
            _currentTarget = _selection.enemy;
        }
        else                        // атаковать противника
            _attack.Punch(target);
    }
    public void GoToPoint(GameObject some = null) // к точке на карте
    {
        agent.stoppingDistance = 0;
        agent.SetDestination(_selection.point);

        if (!_selection.moveToPoint)
        {
            if (_closest != null)   // к ближайшему противнику
            {
                _currentTarget = _closest;
                CurrentState = GoToEnemy;
            }
            else    // к фонтану
            {
                CurrentState = GoToStaticTarget;
                _currentTarget = staticTarget;
            }
        }
        else if (Vector3.Distance(agent.destination, gameObject.transform.position) < agent.stoppingDistance + 2)
            _selection.moveToPoint = false; // переход в автономное управление при достижении точки назначения 
    }
    public void Repair(GameObject some = null)  // восстановление в зоне фонтана
    {
        if (_selection.moveToPoint) // к точке
            CurrentState = GoToPoint;
        else if (_closest != null)  // к ближайшему противнику
        {
            _currentTarget = _closest;
            CurrentState = GoToEnemy;
        }
        else if (Vector3.Distance(agent.destination, gameObject.transform.position) >= agent.stoppingDistance + 1)  // к самому фонтану
        {
            CurrentState = GoToStaticTarget;
            _currentTarget = staticTarget;
        }
        _defence.Repair();  // восстановление
    }
    public override void OnEnable()
    {
        Start();
        EnemiesOnMap.EnemyIn += GetSkill;
    }
    private void GetSkill()
    {
        range = skill.Range;
        speed = skill.Speed;
        agent.stoppingDistance = range;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AzReborn : MonoBehaviour
{
    public GameObject fire;
    public GameObject ice;
    public GameObject cameraPerspective;
    public Camera cameraPers;
    public Camera cameraMain;

    private Vector3 startPoint = new Vector3(37, 2, 0); // место респауна Аза
    private readonly float _resurectionTime = 20;        //время воскрешения
    private bool _loseTrigger;
    private void Start()
    {
        Defence.EnemyWon += () => _loseTrigger = true;
    }
    private void OnDisable()
    {
        if (ice != null && fire != null && cameraPerspective != null)
        {
            ice.GetComponent<IceBolt>().ComaMode();
            fire.GetComponent<FireRain>().ComaMode();   // выключение кнопок и действующего супероружия при уничтожении Аза
            cameraPerspective.GetComponent<CameraPerspective>().SwitchToPersCamera(false, cameraMain); // переключение на основную камеру
            Invoke("Resurection", _resurectionTime);
        }
    }
    private void Resurection()
    {
        if (!_loseTrigger)
        {
            gameObject.transform.position = startPoint;
            gameObject.SetActive(true);
            cameraPerspective.GetComponent<CameraPerspective>().SwitchToPersCamera(true, cameraPers); // переключение на персональную камеру Аза при его респауне
            ice.GetComponent<IceBolt>().Button.interactable = true;     // включение кнопок
            fire.GetComponent<FireRain>().Button.interactable = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public UnitSkills skill;
    public float attackSpeed;
    public float damage;

    private bool _inZone;

    public void GetSkill() // взятие значений из Scriptable object
    {
        damage = skill.Damage;
        attackSpeed = skill.AttackSpeed;
    }
    private void Start()
    {
        _inZone = false;
        GetSkill();
    }
    private void OnEnable()
    {
        Start();
        EnemiesOnMap.EnemyIn += GetSkill;
    }

    public void Punch(GameObject enemy)
    {
        if (!_inZone)       // атака противника с заданной частотой
        {
            StartCoroutine(Beating(enemy));
            _inZone = true;
        }
    }

    IEnumerator Beating(GameObject enemy)
    {
        enemy.GetComponent<Defence>().TakeAHit(gameObject, damage);
        yield return new WaitForSeconds(7 - attackSpeed); // уровней скорострельности: 5
        _inZone = false;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyRespawn : MonoBehaviour
{
    public static event Action WarIsOver;
    public Pool capsulePool;
    public Pool cylinderPool;
    public Pool zazPool;
    public float recreationTime = 30;

    private GameObject _unitPool;
    private int[,] _levels = { { 2, 0 }, { 4, 0 }, { 6, 1 }, { 8, 2 } }; // количество юнитов по уровням
    private int _number = 0;

    private void Start()
    {
        EnemiesOnMap.EnemyOut += NextLevel; // при отсутствии противников через 30 секунд будет запущен следующий уровень
        EvilUnleash(); // первый уровень  
    }
    private void NextLevel()
    {
        if (_number < _levels.GetLength(0)) 
            Invoke("EvilUnleash", recreationTime);
        else
            WarIsOver();
    }
    private void EvilUnleash()
    {
        Generator(_levels[_number, 0], _levels[_number, 1]); // на одном уровне одинаковое количество Капсул и Цилиндров
        _number++;            
    }
    private void Generator(int countCapCyl, int countZaz)
    {
        for (int i = 0; i < countCapCyl; i++) // на одном уровне одинаковое количество Капсул и Цилиндров
        {
            Create(capsulePool);
            Create(cylinderPool);
        }
        for (int i = 0; i < countZaz; i++) // но отличное число Зазов
            Create(zazPool);       
    }
    private void Create(Pool curPool) 
    {
        _unitPool = curPool.Pop(); // вытаскивание юнита из пула
        _unitPool.transform.position = gameObject.transform.position + new Vector3(0, 0, Random.Range(-28,28)); // появление в случайном месте на одной линии
        _unitPool.SetActive(true);
    }
}

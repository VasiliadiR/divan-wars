﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Defence : MonoBehaviour
{
    public static event Action EnemyWon;
    public UnitSkills skill;
    public Image healthBar;
    public Transform valhalla;

    public float armor;
    public float health;

    private float _maxHp;
    private float _gold;
    private float _expirience;
    private bool _inZone;

    public void GetSkill()
    {
        armor = skill.Armor;
        _maxHp = skill.Health;
    }

    void Start()
    {
        health = skill.Health;
        healthBar.fillAmount = health / _maxHp;    // визуализация hp
        GetSkill();                              // взятие значений из Scriptable object
        _gold = skill.Gold;
        _expirience = skill.Expirience;
    }
    public void TakeAHit(GameObject puncher, float damage)
    {
        health -= damage*(1 - armor);               // расчет урона с учетом брони
        healthBar.fillAmount = health / _maxHp;     // визуализация hp
        if (health <= 0)
        {
            if (gameObject.tag == "Enemy")
            {
                Repository.GoldChange(_gold);   // золото за уничтожение противника
                if (puncher.name == "Az")
                    Repository.ExpChange(_expirience);  // опыт за уничтожение противника Азом
            }
            if (gameObject.name == "Divan")
                EnemyWon();
            else
            {
                gameObject.transform.position = valhalla.position; // после "уничтожения" объекты хранятся в области пула, где находятся до инициализации
                gameObject.SetActive(false);
            }
        }
    }
    private void OnEnable()
    {
        Start();
        EnemiesOnMap.EnemyIn += GetSkill;
    }
    public void Repair() // Восстановление здоровья при нахождении у фонтана 
    {
        if (!_inZone)
        {
            StartCoroutine(Healing());
            _inZone = true;
        }
    }
    IEnumerator Healing()
    {
        health += FountainRepair.liveUp;
        healthBar.fillAmount = health / _maxHp;
        yield return new WaitForSeconds(FountainRepair.liveFrequency); 
        _inZone = false;
    }
}

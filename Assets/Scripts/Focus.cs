﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Focus : MonoBehaviour
{
    private Vector3 target;

    void FixedUpdate() // скрипт отвечает за то, чтобы канвасы с HP всегда "смотрели" в камеру. Режимы зависят от включенной в данный момент камеры
    {
        if (CameraPerspective.cam.name == "CameraAz") 
            target = CameraPerspective.cam.transform.position;
        else
        {
            target = CameraPerspective.cam.transform.position;
            target.y *= 100;
            target.z *= 100;
        }

        Vector3 relativePos = target - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        transform.rotation = rotation;
    }
}

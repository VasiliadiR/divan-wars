﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesInSight : MonoBehaviour
{
    public Light lightOn;
    private bool _inZone;

    private void OnMouseDown() // нажатие на юнит
    {
        if (Terminal.selected.Count != 0)
            Terminal.EventForSelected(gameObject);
    }
    public void Shine(GameObject puncher) // подсветка врагов, на которых идут, либо которых атакуют юниты 
    {
        if (!_inZone && gameObject.activeSelf)
        {
            StartCoroutine(Blinking());
            _inZone = true;
        }
    }
    IEnumerator Blinking()
    {
        lightOn.enabled = true;
        yield return new WaitForSeconds(0.5f);
        lightOn.enabled = false;
        _inZone = false;
    }
    private void OnDisable()
    {
        _inZone = false;
        lightOn.enabled = false;
    }
}

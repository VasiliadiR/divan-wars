﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public abstract class BaseStateMachine : MonoBehaviour
{
    public Action<GameObject> CurrentState;
    public Light lightOn;
    public NavMeshAgent agent;
    public UnitSkills skill;

    protected GameObject staticTarget;
    protected GameObject _closest;

    protected Attack _attack;
    protected Defence _defence;

    public float speed;
    public float range;
    protected string _oppositeTag;

    [SerializeField]
    protected string _curMet;
    [SerializeField]
    protected GameObject _currentTarget;

    public abstract void GoToEnemy(GameObject target);
    public abstract void GoToStaticTarget(GameObject staticTarget);
    public abstract void Attack(GameObject target);

    public virtual void Start()
    {
        range = skill.Range;
        speed = skill.Speed;
        agent.stoppingDistance = range;

        _attack = gameObject.GetComponent<Attack>();
        _defence = gameObject.GetComponent<Defence>();

        CurrentState = GoToStaticTarget;
        _currentTarget = staticTarget;
    }
    public virtual void FixedUpdate()
    {
        agent.speed = speed;
        CurrentState(_currentTarget); // обновление текущего состояния автомата
        _curMet = CurrentState.Method.Name; //вывод в инспектор имени текущего метода
        _closest = null;
        GameObject[] units = GameObject.FindGameObjectsWithTag(_oppositeTag); //список друзей/врагов
        _closest = units.OrderBy(x => Vector3.Distance(transform.position, x.transform.position)).FirstOrDefault(); //поиск ближайшего противника

    }
    public virtual void OnEnable()
    {
        Start();
    }
}

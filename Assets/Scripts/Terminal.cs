﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Terminal : MonoBehaviour
{
    public static List<GameObject> selected = new List<GameObject>();

    void OnMouseDown()
    {
        EventForSelected();
    }
    public static void EventForSelected(GameObject enemy = null) //отправление выделенных объектов 
    {
        foreach (var unit in selected)
        {
            var selection = unit.GetComponent<Selection>();
            if (enemy == null)
                selection.ChangeTrigToPoint();  //к точке
            else
                selection.EnemyActive(enemy);   //к противнику
        }
        CameraPerspective.rect = Rect.zero;
        selected.Clear();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FountainRepair : MonoBehaviour
{
    public static float liveCircle = 20;    // радиус действия, в котором будет восстанавливаться здоровье 
    public static float liveFrequency = 5;  // здоровье будет прибавляться на liveUp каждые liveFrequency секунд
    public static float liveUp = 5;
}

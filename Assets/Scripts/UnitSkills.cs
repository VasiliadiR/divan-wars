﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Unit", menuName = "Unit Skills", order = 51)]
public class UnitSkills : ScriptableObject
{
    [SerializeField]
    private float _damage;
    [SerializeField]
    private float _attackSpeed;
    [SerializeField]
    private float _range;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _health;
    [SerializeField]
    private float _armor;
    [SerializeField]
    private float _gold;
    [SerializeField]
    private float _expirience;

    public float startDamage;
    public float startAttackSpeed;
    public float startRange;
    public float startSpeed;
    public float startHealth;
    public float startArmor;

    public float Damage
    {
        get { return _damage; }
        set { _damage = value; }
    }
    public float AttackSpeed
    {
        get { return _attackSpeed; }
        set { _attackSpeed = value; }
    }
    public float Range
    {
        get { return _range; }
        set { _range = value; }
    }
    public float Speed
    {
        get { return _speed; }
        set { _speed = value; }
    }
    public float Health
    {
        get { return _health; }
        set { _health = value; }
    }
    public float Armor
    {
        get{return _armor;}
        set{ _armor = value;}
    }
    public float Gold
    {
        get { return _gold; }
        set { _gold = value; }
    }
    public float Expirience
    {
        get { return _expirience; }
        set { _expirience = value; }
    }
    private void OnEnable()
    {
        startDamage = Damage;
        startAttackSpeed = AttackSpeed;
        startRange = Range;
        startSpeed = Speed;
        startHealth = Health;
        startArmor = Armor;
        Application.quitting += ToStart;    // обнуление изменений в Scriptable Object после выхода из игры
    }
   
    private void ToStart()
    {
        Damage = startDamage;
        AttackSpeed = startAttackSpeed;
        Range = startRange;
        Speed = startSpeed;
        Health = startHealth;
        Armor = startArmor;
    }
    
}

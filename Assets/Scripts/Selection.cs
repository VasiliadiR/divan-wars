﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selection : MonoBehaviour
{
    public bool moveToPoint;
    public Vector3 point;
    public GameObject enemy;
    public Light lightOn;

    private void OnEnable()
    {
        if (gameObject.tag == "Player")
            CameraPerspective.Ract += EntryInRect;
    }
    public void ChangeTrigToPoint() // получения информации из Terminal об отправки к точке
    {
        moveToPoint = true;
        point = CameraPerspective.hit.point;
        lightOn.enabled = false;
        enemy = null;
    }
    public void EnemyActive(GameObject inEnemy) // отправка к помеченному противнику
    {
        enemy = inEnemy;
        lightOn.enabled = false;
    }
    private void OnMouseDown() // нажатие на юнит
    {
        lightOn.enabled = true; // при выборе юнита на нем загорается подсветка
        Terminal.selected.Add(gameObject);
    }
    private void EntryInRect() // выделение нескольких юнитов
    {
        if (this != null)
        {
            Vector2 tmp = new Vector2(Camera.main.WorldToScreenPoint(gameObject.transform.position).x, Screen.height - Camera.main.WorldToScreenPoint(gameObject.transform.position).y);
            if (CameraPerspective.rect.Contains(tmp))
            {
                lightOn.enabled = true;
                Terminal.selected.Add(gameObject);
            }
        }
    }
}

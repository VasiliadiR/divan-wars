﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    public float emissionTime = 20;
    public Pool curPool;
    public float startEmissionTime;

    private GameObject _unitPool;

    private void Start()
    {
        startEmissionTime = emissionTime;
        EnemiesOnMap.EnemyIn += OnGenerate;
        EnemiesOnMap.EnemyOut += OffGenerate;
        Defence.EnemyWon += OffGenerate;
        OnGenerate();
    }
    private void FixedUpdate() // test
    {
        if(Input.GetKeyDown(KeyCode.S))
        {
            _unitPool = curPool.Pop();
            _unitPool.transform.position = gameObject.transform.position;
            _unitPool.SetActive(true);
        }
    }
    private void OnGenerate()   // обучение юнитов происходит при наличии на карте врагов 
    {
        StartCoroutine(Generator());
    }
    private void OffGenerate()  // прекращение обучения - при отсутствии врагов
    {
        StopAllCoroutines();
    }
    private IEnumerator Generator()
    {
        while(true)
        {
            _unitPool = curPool.Pop();
            _unitPool.transform.position = gameObject.transform.position;
            _unitPool.SetActive(true);
            yield return new WaitForSeconds(emissionTime);
        }
    }
}

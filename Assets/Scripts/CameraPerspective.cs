﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraPerspective : MonoBehaviour
{
    public static event Action Ract;
    public static Rect rect;
    public static Ray ray;
    public static RaycastHit hit;
    public static Vector3 Cursor;

    public Canvas mainCanvas;
    public Camera persCamera;
    public Camera mainCamera;
    public static Camera cam;

    public int xBorderRight;
    public int xBorderLeft;
    public int zBorderUp;
    public int zBorderDown;

    public int xCursorRight;
    public int xCursorLeft;
    public int yCursorUp;
    public int yCursorDown;

    private Vector2 startPos;
    private Vector2 endPos;
    private GUIContent cont = new GUIContent();
    private bool drawRect;

    private void Start()
    {
        cam = mainCamera;
    }
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.P))
            SwitchToPersCamera(true, persCamera);
        if (Input.GetKeyDown(KeyCode.M))
            SwitchToPersCamera(false, mainCamera);

        cam = mainCamera.enabled ? mainCamera : persCamera; 

        if (cam != null)
            Raycast();
        Cursor = Input.mousePosition;
        Moving();                       //движение камеры за курсором
    }
    public void SwitchToPersCamera(bool trig, Camera camera)    // переключение камер по кнопкам
    {
        mainCamera.enabled = !trig;
        persCamera.enabled = trig;
        mainCanvas.worldCamera = camera;
    }
    void Moving()
    {
        if (Cursor.x < xCursorLeft && gameObject.transform.position.x > xBorderLeft)
            gameObject.transform.Translate(Vector3.left);

        if (Cursor.x > xCursorRight && gameObject.transform.position.x < xBorderRight)
            gameObject.transform.Translate(Vector3.right);

        if ((Cursor.y > yCursorUp) && gameObject.transform.position.z < zBorderUp)
        {
            gameObject.transform.Translate(Vector3.forward);
            gameObject.transform.Translate(Vector3.up);
        }

        if ((Cursor.y < yCursorDown) && gameObject.transform.position.z > zBorderDown)
        {
            gameObject.transform.Translate(Vector3.down);
            gameObject.transform.Translate(Vector3.back);
        }
    }
   
    void Raycast() // получение координаты точки мыши для направления туда юнита
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        int layerMask = 1 << 1;
        layerMask = ~layerMask;        
        Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask);
    }
    void OnGUI() // выделение объектов прямоугольной областью
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
            drawRect = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            drawRect = false;           
            Ract();
        }
        if (drawRect)
        {
            endPos = Input.mousePosition;
            if (startPos == endPos) return;

            rect = new Rect(Mathf.Min(endPos.x, startPos.x),Screen.height - Mathf.Max(endPos.y, startPos.y),

                            Mathf.Max(endPos.x, startPos.x) - Mathf.Min(endPos.x, startPos.x),
                            Mathf.Max(endPos.y, startPos.y) - Mathf.Min(endPos.y, startPos.y)
                            );

            GUI.Box(rect, cont);
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireRain : SuperWeapon
{
    private float _damage = 15;
    private float _reloadTime = 10;
    private float _area = 15;
    public override GameObject Az { get { return az; } set { az = value; } }
    public override Button Button { get { return fireButton; } set { fireButton = value; } }
    public override float Damage { get { return _damage; } set { _damage = value; } }
    public override float ReloadTime { get { return _reloadTime; } set { _reloadTime= value; } }
    public override float Range { get { return _area; } set { _area = value; } }

    public GameObject az;
    public Button fireButton;

    private void Awake() 
    {
        gameObject.transform.localScale = new Vector3(Range, 0.1f, Range); // размер объекта в соответствии площадью поражения
    }
    public void OnMouseDown() // атака при нажатии на ЛКМ 
    {
        GameObject[] enemiesInFire = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in enemiesInFire)    //если противники в зоне поражения - они будут атакованы
        {
            if (Vector3.Distance(enemy.transform.position, gameObject.transform.position) < Range/2 + 1)
                enemy.GetComponent<Defence>().TakeAHit(Az, Damage);
        }
        _activeTrig = false;
        Button.interactable = false;
        StartCoroutine(ReloadButton());
    }
    private void FixedUpdate()
    {
        if (_activeTrig)
        {            
            if (CameraPerspective.hit.collider != null) // при нахождении оружия на поле боя
            {
                gameObject.transform.position = CameraPerspective.hit.point; // координаты оружия = курсор мыши
                if (Input.GetMouseButtonDown(1))    // нажатие на ПКМ - отмена атаки, удаление оружия из зоны видимости
                    _activeTrig = false;
            }
            else
                gameObject.transform.position = _farAway;
        }
        else
            gameObject.transform.position = _farAway;
    }
    public override void Upgrade() // улучшение оружия
    {
        if (_counterUpgrade < 3)
        {
            base.Upgrade();
            gameObject.transform.localScale = new Vector3(Range, 0.1f, Range);
            _counterUpgrade++;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IceBolt : SuperWeapon
{
    private float _damage = 30;
    private float _reloadTime = 10;
    private float _distance = 20;
    public override GameObject Az { get { return az; } set { az = value; } }
    public override Button Button { get { return iceButton; } set { iceButton = value; } }
    public override float Damage { get { return _damage; } set { _damage = value; } }
    public override float ReloadTime { get { return _reloadTime; } set { _reloadTime = value; } }
    public override float Range { get { return _distance; } set { _distance = value; } }
    public float FrequencyDamage { get; set; } = 2;
    public float SpeedDamage { get; set; } = 2;
    public float Duration { get; set; } = 5;

    public GameObject az;
    public Button iceButton;
    private GameObject[] _iceButtons;
    private Vector3 _addVector = new Vector3(0, 1, 0);
    private Vector3 _point;
    private GameObject _target = null;
    private Collider _collider;

    private EnemyStateMachine _stateMachine;
    private Attack _attack;
    private Defence _defence;

    private float _startSpeed;
    private float _startFrequency;

    public void OnMouseDown() // захват цели и сближение с противником при нажатии на ЛКМ 
    {      
        if (_collider.tag == "Enemy") // невозможность атаковать союзника
        {
            Terminal.selected.Clear();
            Terminal.selected.Add(Az);
            _target = GameObject.Find(_collider.name);
            Terminal.EventForSelected(_target);         //отправление Аза к противнику
            _stateMachine = _target.GetComponent<EnemyStateMachine>();
            _defence = _target.GetComponent<Defence>();
            _attack = _target.GetComponent<Attack>();

            Cursor.visible = true;
            gameObject.transform.position = _farAway;
            _activeTrig = false;
        }
    }
    private void Attack()
    {
        _startFrequency = _attack.attackSpeed; // сохранение изначальных параметров, которые будут возвращены после окончания воздействия
        _startSpeed = _stateMachine.speed;
        _defence.TakeAHit(Az, Damage);      // нанесение прямого урона
        _attack.attackSpeed -= FrequencyDamage; // снижение скорости атаки
        _stateMachine.speed -= SpeedDamage; // снижение скорости
        _target = null;
        Button.interactable = false;
        Invoke("ReturnSkills", Duration);
        StartCoroutine(ReloadButton());
    }
    private void FixedUpdate()
    {
        if (_target != null && Vector3.Distance(Az.transform.position, _target.transform.position) < Range) 
        {
            Attack(); // выполняется при сближении с противником
        }
        if (_activeTrig)
        {
            _point = CameraPerspective.hit.point;
            _collider = CameraPerspective.hit.collider;

            if (_collider != null)  // при нахождении оружия на поле боя
            {
                gameObject.transform.position = _point + _addVector;    // координаты оружия = курсор мыши
                Cursor.visible = false;
                if (Input.GetMouseButtonDown(1)) // нажатие на ПКМ - отмена атаки, удаление оружия из зоны видимости
                {
                    Cursor.visible = true;
                    _activeTrig = false;
                }
            }
            else
            {
                gameObject.transform.position = _farAway;
                Cursor.visible = true;
            }
        }
        else
        {
            gameObject.transform.position = _farAway;
            Cursor.visible = true;
        }
    }
    public void ReturnSkills() // возвращение скорости и скорости атаки после окончания воздействия
    {
        _attack.attackSpeed = _startFrequency;
        _stateMachine.speed = _startSpeed;
    }
    public override void Upgrade() // улучшение оружия
    {
        if (_counterUpgrade < 3)
        {
            base.Upgrade();
            FrequencyDamage += 1;
            SpeedDamage += 1;
            Duration += 1;
            _counterUpgrade++;
        }
    }
}

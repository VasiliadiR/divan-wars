﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class SuperWeapon : MonoBehaviour
{
    public abstract float Damage { get;set; }
    public abstract float Range { get; set; }
    public abstract float ReloadTime { get; set; }
    public abstract GameObject Az { get; set; }
    public abstract Button Button { get; set; } // кнопка, нажатие на которую запускает оружие

    protected Vector3 _farAway = new Vector3(100, 100, 0); // удаление оружия за пределы видимости
    protected float _priceOfSWSkill = 20;
    protected bool _activeTrig; // флаг, отвечающий за нахождения оружия в пределах видимости
    protected int _counterUpgrade = 0;



    public virtual void Upgrade() // базовое улучшение оружия 
    {
        Repository.ExpChange(-_priceOfSWSkill);
        Damage += 8;
        Range += 5;
        ReloadTime -= 2;
    }
    public IEnumerator ReloadButton() // перезарядка оружия. Метод делает доступным нажатие на кнопку спустя время
    {
        yield return new WaitForSeconds(ReloadTime);
        Button.interactable = true;
    }
    public virtual void ComaMode() // кнопки неавктивны при отсутствии Аза, вызванное оружие, будет удалено за пределы видимости
    {
        StopAllCoroutines();
        Button.interactable = false;
        _activeTrig = false;
    }
    public virtual void TrigOn() // вызов оружия через кнопку
    {
        _activeTrig = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillUp : MonoBehaviour
{
    public Text DamageText;
    public Text AttackSpeedText;
    public Text RangeText;
    public Text SpeedText;
    public Text HealthText;
    public Text ArmorText;
    public Text EmissionText;
    public PlayerRespawn respawn;
    public UnitSkills skill;

    private readonly float _maxUp = 5;
    private readonly float _priceOfSkill = 10;

    private readonly float _damageUp = 1;
    private readonly float _attackSpeedUp = 1;
    private readonly float _rangeUp = 2;
    private readonly float _speedUp = 1;
    private readonly float _healthUp = 5;
    private readonly float _armorUp = 0.1f;
    private readonly float _emissionUp = 1;

    bool Limit(float a, float b, float c) => (a < (b * _maxUp + c));    // каждый скил нельзя увеличить более, чем на пять раз

    private void Start()
    {
        DamageText.text = skill.Damage.ToString();
        AttackSpeedText.text = skill.AttackSpeed.ToString();
        RangeText.text = skill.Range.ToString();
        SpeedText.text = skill.Speed.ToString();
        HealthText.text = skill.Health.ToString();
        ArmorText.text = skill.Armor.ToString();
        EmissionText.text = respawn.emissionTime.ToString();    
    }
    public void DamageUp()
    {
        if (Limit(skill.Damage, _damageUp, skill.startDamage))
        {
            Repository.GoldChange(-_priceOfSkill);
            skill.Damage += _damageUp;
            DamageText.text = skill.Damage.ToString();
        }
    }
    public void AttackSpeedUp()
    {
        if (Limit(skill.AttackSpeed, _attackSpeedUp, skill.startAttackSpeed))
        {
            Repository.GoldChange(-_priceOfSkill);
            skill.AttackSpeed += _attackSpeedUp;
            AttackSpeedText.text = skill.AttackSpeed.ToString();
        }
    }
    public void RangeUp()
    {
        if (Limit(skill.Range, _rangeUp, skill.startRange))
        {
            Repository.GoldChange(-_priceOfSkill);
            skill.Range += _rangeUp;
            RangeText.text = skill.Range.ToString();
        }
    }
    public void SpeedUp()
    {
        if (Limit(skill.Speed, _speedUp, skill.startSpeed))
        {
            Repository.GoldChange(-_priceOfSkill);
            skill.Speed += _speedUp;
            SpeedText.text = skill.Speed.ToString();
        }
    }
    public void HealthUp()
    {
        if (Limit(skill.Health, _healthUp, skill.startHealth))
        {
            Repository.GoldChange(-_priceOfSkill);
            skill.Health += _healthUp;
            HealthText.text = skill.Health.ToString();
        }
    }
    public void ArmorUp()
    {
        if (Limit(skill.Armor, _armorUp, skill.startArmor))
        {
            Repository.GoldChange(-_priceOfSkill);
            skill.Armor += _armorUp;
            ArmorText.text = skill.Armor.ToString();
        }
    }
    public void EmissionUp()
    {
        if(Limit(-respawn.emissionTime, _emissionUp, -respawn.startEmissionTime))
        {
            Repository.GoldChange(-_priceOfSkill);
            respawn.emissionTime -= _emissionUp;
            EmissionText.text = respawn.emissionTime.ToString();
        }
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStateMachine : BaseStateMachine
{
    public override void Start()
    {
        base.Start();
        staticTarget = GameObject.Find("Divan");
        _oppositeTag = "Player"; // выбор поискового тэга
    }
    public override void GoToStaticTarget(GameObject staticTarget) // к статичному объекту (дивану)
    {
        agent.stoppingDistance = range;
        agent.SetDestination(staticTarget.transform.position);

        if (_closest != null)  // к ближайшему противнику
        {
            _currentTarget = _closest;
            CurrentState = GoToEnemy;
        }
        else if (Vector3.Distance(agent.destination, gameObject.transform.position) < agent.stoppingDistance)   //сближение со статическим объектом
        {
            CurrentState = Attack;
            _currentTarget = staticTarget;
        }
    }
    public override void GoToEnemy(GameObject target) // к ближайшему или выбранному противнику
    {
        _currentTarget = _closest;
        agent.stoppingDistance = range;
        agent.SetDestination(target.transform.position);

        if (_closest == null)  // к ближайшему противнику
        {
            CurrentState = GoToStaticTarget;
            _currentTarget = staticTarget;
        }
        else if (Vector3.Distance(agent.destination, gameObject.transform.position) < agent.stoppingDistance + 1)   // при достаточном сближении с целью - атаковать
        {
            CurrentState = Attack;
            _currentTarget = _closest;
        }
    }
    public override void Attack(GameObject target)  // атака противника
    {
        agent.SetDestination(target.transform.position);

        if (Vector3.Distance(agent.destination, gameObject.transform.position) >= agent.stoppingDistance + 1)  // к тому же противнику при увеличении дистанции
        {
            CurrentState = GoToEnemy;
            _currentTarget = target;
        }
        else if (target == staticTarget && _closest != null)   // диван атакуется врагами только при отсутствии юнитов игрока на поле
        {
            CurrentState = GoToEnemy;
            _currentTarget = _closest;
        }
        else                        // атаковать противника
            _attack.Punch(target);
    }
}

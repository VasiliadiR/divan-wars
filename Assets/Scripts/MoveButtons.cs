﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveButtons : MonoBehaviour
{
    public GameObject target;
    public GameObject button;
    private RectTransform _menuRect;
    private RectTransform _targetRect;
    private Vector3 _startPoint;
    private Vector3 _targetPoint;
    private bool _switch;
    private bool _switchEnemies;

    private void Start()
    {
        _menuRect = gameObject.GetComponent<RectTransform>();
        _targetRect = target.GetComponent<RectTransform>();
        _startPoint = _menuRect.localPosition;
        _targetPoint = _targetRect.localPosition;
        EnemiesOnMap.EnemyIn += Invisible;
        EnemiesOnMap.EnemyOut += Visible;
    }
    private void Visible()  // появление кнопки меню улучшения скилов при исчезновении противников
    {
        _switchEnemies = true;
        button.SetActive(true);
    }
    private void Invisible()    // скрытие меню улучшения скилов при появлении противников
    {
        StartCoroutine(Moving(_startPoint, _switchEnemies));
        _switch = true;
        _switchEnemies = false;

    }
    public void Move()  // появление или исчезновение меню улучшения скилов при нажатии кнопки
    {
        if (!_switch)
            StartCoroutine(Moving(_targetPoint));
        else
            StartCoroutine(Moving(_startPoint));
    }
    private IEnumerator Moving(Vector3 targetPoint, bool switchEnemies = false)
    {
        while (GreatDistance(_menuRect.localPosition, _startPoint, (x, y) => Vector3.Distance(x, y)))
        {
            _menuRect.localPosition = Vector3.MoveTowards(_menuRect.localPosition, targetPoint, 1000 * Time.deltaTime);
            yield return null;
        }
        _switch = _switch ? false : true;
        if (switchEnemies)
            button.SetActive(false);
    }
    bool GreatDistance(Vector3 start, Vector3 finish, Func<Vector3, Vector3, float> unit)
    {
        if (!_switch)
            return unit(start, finish) < 330;
        else
            return unit(start, finish) > 0;
    }
}
